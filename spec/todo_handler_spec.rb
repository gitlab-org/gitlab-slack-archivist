require 'todo_handler'

RSpec.describe TodoHandler do
  let(:env) do
    {
      # The defaults are used for creating new VCR recordings. Most spec runs
      # won't need them.
      'GITLAB_API_TOKEN' => ENV.fetch('GITLAB_API_TOKEN', '<GITLAB_API_TOKEN>'),
      'SLACK_API_TOKEN' => ENV.fetch('SLACK_API_TOKEN', '<SLACK_API_TOKEN>'),
      'GITLAB_GROUP_ID' => described_class::DEFAULT_GITLAB_GROUP_ID
    }
  end

  let(:stub_todo) { double(:todo) }

  subject(:todo_handler) { described_class.new(env) }

  describe '#initialize' do
    context 'when the env is missing a GitLab token' do
      it 'raises an error' do
        expect { described_class.new(env.slice('SLACK_API_TOKEN', 'GITLAB_GROUP_ID')) }.
          to raise_error(described_class::Error)
      end
    end

    context 'when the env is missing a Slack token' do
      it 'raises an error' do
        expect { described_class.new(env.slice('GITLAB_API_TOKEN', 'GITLAB_GROUP_ID')) }.
          to raise_error(described_class::Error)
      end
    end

    context 'when the env is missing a GitLab group' do
      it 'uses the default group' do
        instance = described_class.new(env.slice('GITLAB_API_TOKEN', 'SLACK_API_TOKEN'))

        expect(instance.gitlab_group_id).to eq(described_class::DEFAULT_GITLAB_GROUP_ID)
      end
    end

    context 'with valid config' do
      it 'creates clients from from the tokens given' do
        expect(todo_handler.gitlab_client).to be_instance_of(GitlabApi)
        expect(todo_handler.gitlab_client.token).to eq(env['GITLAB_API_TOKEN'])
        expect(todo_handler.slack_client).to be_instance_of(SlackApi)
        expect(todo_handler.slack_client.token).to eq(env['SLACK_API_TOKEN'])
      end

      it 'uses the GitLab group ID from the environment' do
        expect(todo_handler.gitlab_group_id).to eq(env['GITLAB_GROUP_ID'])
      end
    end
  end

  describe '#archive!' do
    it 'calls #handle_todo! on each todo in the list' do
      stub_todo_2 = double(:todo)

      allow(todo_handler).to receive(:todo_list).and_return([stub_todo, stub_todo_2])

      expect(todo_handler).to receive(:handle_todo!).with(stub_todo).ordered
      expect(todo_handler).to receive(:handle_todo!).with(stub_todo_2).ordered

      todo_handler.archive!
    end
  end

  describe '#handle_todo!' do
    context 'when the todo does not have a valid target_type' do
      it 'marks the todo as done without commenting' do
        allow(todo_handler).to receive(:todo_target_valid?).with(stub_todo).and_return(false)

        expect(todo_handler).to receive(:mark_todo_as_done!).with(stub_todo)
        expect(todo_handler).not_to receive(:reply_with_slack_message!)

        todo_handler.handle_todo!(stub_todo)
      end
    end

    context 'when the todo does not have a valid action' do
      it 'marks the todo as done without commenting' do
        allow(todo_handler).to receive(:todo_target_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_action_valid?).with(stub_todo).and_return(false)

        expect(todo_handler).to receive(:mark_todo_as_done!).with(stub_todo)
        expect(todo_handler).not_to receive(:reply_with_slack_message!)

        todo_handler.handle_todo!(stub_todo)
      end
    end

    context 'when the todo does not have a valid author' do
      it 'marks the todo as done without commenting' do
        allow(todo_handler).to receive(:todo_target_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_action_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_author_allowed?).with(stub_todo).and_return(false)

        expect(todo_handler).to receive(:mark_todo_as_done!).with(stub_todo)
        expect(todo_handler).not_to receive(:reply_with_slack_message!)

        todo_handler.handle_todo!(stub_todo)
      end
    end

    context 'when the todo does not have any Slack messages' do
      it 'marks the todo as done without commenting' do
        allow(todo_handler).to receive(:todo_target_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_action_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_author_allowed?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:slack_message_for).with(stub_todo).and_return(nil)

        expect(todo_handler).to receive(:mark_todo_as_done!).with(stub_todo)
        expect(todo_handler).not_to receive(:reply_with_slack_message!)

        todo_handler.handle_todo!(stub_todo)
      end
    end

    context 'when the todo has a valid Slack message' do
      it 'replies with that message' do
        slack_message = double(:slack_message)

        allow(todo_handler).to receive(:todo_target_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_action_valid?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:todo_author_allowed?).with(stub_todo).and_return(true)
        allow(todo_handler).to receive(:slack_message_for).with(stub_todo).and_return(slack_message)

        expect(todo_handler).not_to receive(:mark_todo_as_done!)
        expect(todo_handler).to receive(:reply_with_slack_message!).with(stub_todo, slack_message)

        todo_handler.handle_todo!(stub_todo)
      end
    end
  end

  describe '#todo_target_valid?' do
    context 'when the todo targets a type in API_TYPES' do
      it 'returns true' do
        described_class::API_TYPES.keys.each do |type|
          expect(todo_handler.todo_target_valid?('target_type' => type)).to eq(true)
        end
      end
    end

    context 'when the todo targets any other type' do
      it 'returns false' do
        expect(todo_handler.todo_target_valid?('target_type' => 'Commit')).to eq(false)
      end
    end
  end

  describe '#todo_action_valid?' do
    context 'when the action is mentioned' do
      it 'returns true' do
        expect(todo_handler.todo_action_valid?('action_name' => 'mentioned')).to eq(true)
      end
    end

    context 'when the action is directly_addressed' do
      it 'returns true' do
        expect(todo_handler.todo_action_valid?('action_name' => 'directly_addressed')).to eq(true)
      end
    end

    context 'when the action is anything else' do
      it 'returns false' do
        expect(todo_handler.todo_action_valid?('action_name' => 'marked')).to eq(false)
      end
    end
  end

  describe '#todo_author_allowed?', :vcr do
    context 'when the author is a member of the GitLab group' do
      it 'returns true' do
        todo = { 'author' => { 'id' => 1 } } # @sytses

        expect(todo_handler.todo_author_allowed?(todo)).to eq(true)
      end
    end

    context 'when the author is not a member of the GitLab group' do
      it 'returns false' do
        todo = { 'author' => { 'id' => 1359965 } } # @smcgivern-test

        expect(todo_handler.todo_author_allowed?(todo)).to eq(false)
      end
    end

    it 'memoises the result in the instance' do
      todo = { 'author' => { 'id' => 1 } } # @sytses

      todo_handler.todo_author_allowed?(todo)

      expect(todo_handler.gitlab_client).not_to receive(:get)

      expect(todo_handler.todo_author_allowed?(todo)).to eq(true)
    end
  end

  describe '#slack_message_for' do
    context 'when there are no Slack messages' do
      it 'returns nil' do
        allow(todo_handler).to receive(:slack_messages_for).with(stub_todo).and_return([])

        expect(todo_handler.slack_message_for(stub_todo)).to be_nil
      end
    end

    context 'when there are Slack messages' do
      it 'returns the formatted messages as a string with author and time' do
        allow(todo_handler).
          to receive(:slack_messages_for).with(stub_todo).
               and_return([{
                             url: 'https://gitlab.slack.com/archives/C72HPNV97/p1556876971077400',
                             author: 'Sean McGivern',
                             time: Time.at(1556876971).utc,
                             message: 'Test'
                           }] * 2)

        expect(todo_handler.slack_message_for(stub_todo)).
          to eq("Sean McGivern wrote at 2019-05-03 09:49:31 UTC:\n\n>>>\nTest\n>>>\n\n---\n\nSean McGivern wrote at 2019-05-03 09:49:31 UTC:\n\n>>>\nTest\n>>>")
      end
    end
  end

  describe '#slack_messages_for', :vcr, :slack do
    context 'when there are no Slack channel messages in the body' do
      let(:body) do
        # Doesn't match because it's a DM, not in a channel
        'https://gitlab.slack.com/archives/D10N476NP/p1557785704000700'
      end

      it 'returns an empty array' do
        expect(todo_handler.slack_messages_for('body' => body)).to eq([])
      end
    end

    context 'when there are Slack messages in the body' do
      let(:body) do
        "https://gitlab.slack.com/archives/C72HPNV97/p1556876971077400\nhttps://gitlab.slack.com/archives/C72HPNV97/p1557233726092300?thread_ts=1557233606.092000&cid=C72HPNV97"
      end

      it 'returns an array of hashes of the message URL, text, author, and time' do
        expect(todo_handler.slack_messages_for('body' => body)).
          to eq([
                  {
                    url: 'https://gitlab.slack.com/archives/C72HPNV97/p1556876971077400',
                    author: 'Sean McGivern',
                    time: Time.at(1556876971).utc,
                    message: 'I added a very early outline of our next group conversation in <https://gitlab.com/gitlab-org/group-conversations/merge_requests/17> :slightly_smiling_face:'
                  },
                  {
                    url: 'https://gitlab.slack.com/archives/C72HPNV97/p1557233726092300?thread_ts=1557233606.092000&cid=C72HPNV97',
                    author: 'Sean McGivern',
                    time: Time.at(1557233726).utc,
                    message: "once we have <https://gitlab.com/gitlab-org/gitlab-ee/issues/9121>, I won't need to say things like 'I'm not sure what the prioritisation' :slightly_smiling_face:"
                  }
                ])
      end
    end
  end

  describe '#note_api_url_for', :vcr do
    context 'when the todo is on an epic' do
      let(:epic_url) { 'https://gitlab.com/groups/gitlab-org/-/epics/149' }

      let(:todo_base) do
        {
          'target_type' => 'Epic',
          'target' => { 'id' => 266 },
          'group' => { 'id' => 9970 }
        }
      end

      context 'when the todo is on a note' do
        it 'finds the right URL, including the discussion ID' do
          todo = todo_base.merge('target_url' => epic_url + '#note_128130160')

          expect(todo_handler.note_api_url_for(todo)).
            to eq('groups/9970/epics/266/discussions/7c7b1c065c098ceff2ea0355197b53a3ea12b0e0/notes')
        end
      end

      it 'finds the right URL' do
        todo = todo_base.merge('target_url' => epic_url)

        expect(todo_handler.note_api_url_for(todo)).
          to eq('groups/9970/epics/266/notes')
      end
    end

    context 'when the todo is on an issue' do
      let(:issue_url) { 'https://gitlab.com/gitlab-org/gitlab-slack-archivist/issues/5' }

      let(:todo_base) do
        {
          'target_type' => 'Issue',
          'target' => { 'iid' => 5 },
          'project' => { 'id' => 12212610 }
        }
      end

      context 'when the todo is on a note' do
        it 'finds the right URL, including the discussion ID' do
          todo = todo_base.merge('target_url' => issue_url + '#note_167547546')

          expect(todo_handler.note_api_url_for(todo)).
            to eq('projects/12212610/issues/5/discussions/da351d7f4a3ad00bfa19b65bd37d376c8a5ca0a8/notes')
        end
      end

      it 'finds the right URL' do
        todo = todo_base.merge('target_url' => issue_url)

        expect(todo_handler.note_api_url_for(todo)).
            to eq('projects/12212610/issues/5/notes')
      end
    end
  end
end
