require './lib/todo_handler'

# Example invocation:
# GITLAB_GROUP_ID=7 GITLAB_API_ENDPOINT=http://localhost:3001/api/v4 GITLAB_API_TOKEN=dxxysh8yR4FUzvQiC5As bundle exec ruby archivist.rb

begin
  TodoHandler.new(ENV).archive!
rescue TodoHandler::Error => e
  STDERR.puts e.message
  exit 1
rescue => e
  STDERR.puts e.class
  STDERR.puts e.backtrace
  exit 1
end
