require_relative 'gitlab_api'
require_relative 'slack_api'

class TodoHandler
  Error = Class.new(StandardError)
  UnhandledTodo = Class.new(StandardError)

  DEFAULT_GITLAB_GROUP_ID = '6543'

  API_TYPES = {
    'Epic' => 'epics',
    'Issue' => 'issues',
    'MergeRequest' => 'merge_requests'
  }

  SLACK_URL = %r{
    https://gitlab.slack.com/archives/
    (?<channel>C[A-Z0-9]+) # only match messages in public channels
    /
    p(?<ts_first>\d{10})(?<ts_last>\d+) # no dot
    (\?
      thread_ts=(?<thread_ts_first>\d{10}).(?<thread_ts_last>\d+) # dot
      &cid=
      C[A-Z0-9]+ # the same as the one earlier in the URL, no need to capture
    )?
  }x

  attr_accessor :gitlab_group_id, :gitlab_client, :slack_client

  def initialize(env)
    gitlab_api_token = env['GITLAB_API_TOKEN']
    slack_api_token = env['SLACK_API_TOKEN']

    raise Error.new('No GITLAB_API_TOKEN found') unless gitlab_api_token
    raise Error.new('No SLACK_API_TOKEN found') unless slack_api_token

    @gitlab_client = GitlabApi.new(token: gitlab_api_token)
    @slack_client = SlackApi.new(token: slack_api_token)
    @gitlab_group_id = env.fetch('GITLAB_GROUP_ID', DEFAULT_GITLAB_GROUP_ID)
  end

  def archive!
    todo_list.each(&method(:handle_todo!))
  end

  def todo_list
    gitlab_client.get('todos', auth: true)
  end

  def handle_todo!(todo)
    raise UnhandledTodo unless todo_target_valid?(todo)
    raise UnhandledTodo unless todo_action_valid?(todo)
    raise UnhandledTodo unless todo_author_allowed?(todo)

    slack_message = slack_message_for(todo)

    raise UnhandledTodo unless slack_message

    reply_with_slack_message!(todo, slack_message)
  rescue UnhandledTodo
    mark_todo_as_done!(todo)
  end

  def reply_with_slack_message!(todo, slack_message)
    gitlab_client.post(note_api_url_for(todo), body: { body: slack_message })
  end

  def mark_todo_as_done!(todo)
    gitlab_client.post("todos/#{todo['id']}/mark_as_done", body: nil)
  end

  def todo_target_valid?(todo)
    API_TYPES.key?(todo['target_type'])
  end

  def todo_action_valid?(todo)
    todo['action_name'] == 'mentioned' || todo['action_name'] == 'directly_addressed'
  end

  def todo_author_allowed?(todo)
    @allowed_authors ||= Hash.new do |hash, author_id|
      url = "groups/#{gitlab_group_id}/members/#{author_id}"

      hash[author_id] = !!gitlab_client.get(url, auth: true, check_response_code: false)['id']
    end

    @allowed_authors[todo['author']['id']]
  end

  def slack_message_for(todo)
    messages = slack_messages_for(todo)

    return if messages.empty?

    messages.map do |slack_message|
      "#{slack_message[:author]} wrote at #{slack_message[:time]}:\n\n>>>\n#{slack_message[:message]}\n>>>"
    end.join("\n\n---\n\n")
  end

  def slack_messages_for(todo)
    todo['body'].split.map do |word|
      captures = SLACK_URL.match(word)

      next unless captures

      if captures['thread_ts_first']
        method = 'conversations.replies'
        latest = "#{captures['thread_ts_first']}.#{captures['thread_ts_last']}"
      else
        method = 'conversations.history'
        latest = "#{captures['ts_first']}.#{captures['ts_last']}"
      end

      message = slack_client.get_single_message(method, captures['channel'], captures['ts_first'], captures['ts_last'], latest)

      {
        url: captures[0],
        message: message['text'],
        author: message['author_name'],
        time: Time.at(message['ts'].to_i).utc
      }
    end.compact
  end

  def note_api_url_for(todo)
    api_type = API_TYPES[todo['target_type']]
    note_id = todo['target_url'].match(/#note_(\d+)$/)&.captures&.first

    if todo['group']
      target_api_url = "groups/#{todo['group']['id']}"
      target_id = todo['target']['id']
    else
      target_api_url = "projects/#{todo['project']['id']}"
      target_id = todo['target']['iid']
    end

    target_api_url += "/#{api_type}/#{target_id}"

    if note_id
      discussion = gitlab_client.get("#{target_api_url}/discussions", auth: true).find do |discussion_hash|
        discussion_hash['notes'].any? { |note| note['id'].to_s == note_id }
      end
    end

    if discussion
      target_api_url += "/discussions/#{discussion['id']}"
    end

    target_api_url + '/notes'
  end
end
